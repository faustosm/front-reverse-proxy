#!/bin/bash
docker stack rm  front_reverse_proxy

export $(cat setup.container | sed '/^#/d; s/\r//' | xargs) && docker stack deploy --compose-file docker-compose.yml --resolve-image=never front_reverse_proxy --with-registry-auth
